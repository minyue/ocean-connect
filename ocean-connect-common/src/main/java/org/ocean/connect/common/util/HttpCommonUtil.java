/**
 * 
 */
package org.ocean.connect.common.util;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

/**
 * @author leiruiqi
 *
 */
public class HttpCommonUtil {

	/**  
    私有IP：A类  10.0.0.0-10.255.255.255  
           B类  172.16.0.0-172.31.255.255  
           C类  192.168.0.0-192.168.255.255  
    当然，还有127这个网段是环回地址  
    **/  
    static long aBegin = getIpNum("10.0.0.0");   
    static long aEnd = getIpNum("10.255.255.255");   
    static long bBegin = getIpNum("172.16.0.0");   
    static long bEnd = getIpNum("172.31.255.255");   
    static long cBegin = getIpNum("192.168.0.0");   
    static long cEnd = getIpNum("192.168.255.255");  
    
	public static boolean isAjaxRequest(HttpServletRequest request){
		String requestedWith=request.getHeader("x-requested-with");  
		if(StringUtils.isEmpty(requestedWith)){
			return false;
		}else if(StringUtils.isNotEmpty(requestedWith) && requestedWith.equals("XMLHttpRequest")){
			return true;
		}
		return false;
	}

	public static String getRemoteIp(HttpServletRequest request) {
		String remoteIp = request.getHeader("x-forwarded-for");
		if (remoteIp == null || remoteIp.isEmpty()
				|| "unknown".equalsIgnoreCase(remoteIp)) {
			remoteIp = request.getHeader("X-Real-IP");
		}
		if (remoteIp == null || remoteIp.isEmpty()
				|| "unknown".equalsIgnoreCase(remoteIp)) {
			remoteIp = request.getHeader("Proxy-Client-IP");
		}
		if (remoteIp == null || remoteIp.isEmpty()
				|| "unknown".equalsIgnoreCase(remoteIp)) {
			remoteIp = request.getHeader("WL-Proxy-Client-IP");
		}
		if (remoteIp == null || remoteIp.isEmpty()
				|| "unknown".equalsIgnoreCase(remoteIp)) {
			remoteIp = request.getHeader("HTTP_CLIENT_IP");
		}
		if (remoteIp == null || remoteIp.isEmpty()
				|| "unknown".equalsIgnoreCase(remoteIp)) {
			remoteIp = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (remoteIp == null || remoteIp.isEmpty()
				|| "unknown".equalsIgnoreCase(remoteIp)) {
			remoteIp = request.getRemoteAddr();
		}
		if (remoteIp == null || remoteIp.isEmpty()
				|| "unknown".equalsIgnoreCase(remoteIp)) {
			remoteIp = request.getRemoteHost();
		}
		if (remoteIp != null && remoteIp.indexOf(",") != -1) {
			remoteIp = remoteIp.substring(remoteIp.lastIndexOf(",") + 1,
					remoteIp.length()).trim();
		}
		return remoteIp;
	}
	
	public static boolean isInnerIP(String ipAddress){   
        boolean isInnerIp = false;   
        long ipNum = getIpNum(ipAddress);   
         
        isInnerIp = isInner(ipNum,aBegin,aEnd) || isInner(ipNum,bBegin,bEnd) || isInner(ipNum,cBegin,cEnd) || ipAddress.equals("127.0.0.1");   
        return isInnerIp;              
	} 
	private static long getIpNum(String ipAddress) {   
	    String [] ip = ipAddress.split("\\.");   
	    long a = Integer.parseInt(ip[0]);   
	    long b = Integer.parseInt(ip[1]);   
	    long c = Integer.parseInt(ip[2]);   
	    long d = Integer.parseInt(ip[3]);   
	  
	    long ipNum = a * 256 * 256 * 256 + b * 256 * 256 + c * 256 + d;   
	    return ipNum;   
	}  
	private static boolean isInner(long userIp,long begin,long end){   
	     return (userIp>=begin) && (userIp<=end);   
	} 
}
