/*
 *Copyright © 2015 zhaorongbao
 *招融宝
 *http://zhaorongbao.com
 *All rights reserved.
 */
package org.ocean.connect.common.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateFormatUtil {
	/**
	 * format 日期类型 格式化成字符串类型
	 */
	public static String format(Date date, String pattern) {
		return format(date, pattern, Locale.getDefault());
	}

	/**
	 * format 日期类型 格式化成字符串类型
	 */
	public static String format(Date date, String pattern, Locale locale) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern,
				locale);
		String format = simpleDateFormat.format(date);
		return format;
	}

}
