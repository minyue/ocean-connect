/**
 * 
 */
package org.ocean.connect.common.util;

/**
 * @author leiruiqi
 *
 */
public class InnerAuthToken {

	private String domain;
	
	private String userType;
	
	private Long time;

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}
	
	
	
	
}
