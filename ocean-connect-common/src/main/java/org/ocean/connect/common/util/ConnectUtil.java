package org.ocean.connect.common.util;

public class ConnectUtil {

	/**
	 * 16位的密钥
	 */
	private static String key="tianxiao-cc-1234";
	
	public static String getTokenByBizName(String bizName) throws Exception {
		return AES.Encrypt(bizName, key);
	}

	public static String getBizNameByToken(String token) throws Exception {
		return AES.Decrypt(token, key);
	}
	
	public static void main(String[] args) {
		String bizName = "abc123";
		String token = null;
		try {
			token = getTokenByBizName(bizName);
			System.out.println(token);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		
		String deName = null;
		
		try {
			deName  = getBizNameByToken(token);
			System.out.println(deName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}