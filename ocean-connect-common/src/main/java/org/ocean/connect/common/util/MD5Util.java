
package org.ocean.connect.common.util;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MD5Util {

	public static String sha1(String content) {
		if(content==null){
			throw new RuntimeException("content is null");
		}
		MessageDigest md;
		String sha1str = "";
		try {
			md = MessageDigest.getInstance("SHA1");
			md.update(content.getBytes());
			sha1str = byte2hex(md.digest());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return sha1str;
	}

	public static void main(String[] args) {
		System.out.println(sha1(DateFormatUtil.format(new Date(),
				"yyyyMMddHHmmssSSS")));
	}

	public static String md5(String plaintxt) {
		MessageDigest md;
		String MD5str = "";
		try {
			md = MessageDigest.getInstance("MD5");
			md.update(plaintxt.getBytes());
			MD5str = byte2hex(md.digest());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return MD5str;
	}

	/**
	 * java字节码转字符串
	 * 
	 * @param b
	 * @return
	 */
	public static String byte2hex(byte[] b) { // 一个字节的数，

		// 转成16进制字符串

		String hs = "";
		String tmp = "";
		for (int n = 0; n < b.length; n++) {
			// 整数转成十六进制表示

			tmp = (java.lang.Integer.toHexString(b[n] & 0XFF));
			if (tmp.length() == 1) {
				hs = hs + "0" + tmp;
			} else {
				hs = hs + tmp;
			}
		}
		tmp = null;
		return hs.toUpperCase(); // 转成大写

	}

	public static int CompareIp(String ip1, String ip2) throws Exception {
		String[] strIP1 = ip1.split("\\.");
		String[] strIP2 = ip2.split("\\.");
		if (strIP1.length != 4 || strIP2.length != 4) {
			throw new Exception("ip地址不合法！");
		}
		for (int i = 0; i < strIP1.length; i++) {
			if (Integer.valueOf(strIP1[i]) > Integer.valueOf(strIP2[i])) {
				return 1;
			} else if (Integer.valueOf(strIP1[i]) < Integer.valueOf(strIP2[i])) {
				return -1;
			}
		}
		return 0;
	}

	public static String dateToString3(String datestr) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateString = datestr;
		String mDateTime1 = "";
		try {
			Date date = df.parse(dateString);
			SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
			mDateTime1 = formatter.format(date);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}

		return mDateTime1;
	}

	/**
	 * str2Ip String类型IP转换为int类型
	 * 
	 * @param ip
	 * @return
	 * @throws UnknownHostException
	 */
	public static int str2Ip(String ip) throws UnknownHostException {
		InetAddress address = InetAddress.getByName(ip);// 在给定主机名的情况下确定主机的 IP 址。
		byte[] bytes = address.getAddress();// 返回此 InetAddress 对象的原始 IP 地址
		int a, b, c, d;
		a = byte2int(bytes[0]);
		b = byte2int(bytes[1]);
		c = byte2int(bytes[2]);
		d = byte2int(bytes[3]);
		int result = (a << 24) | (b << 16) | (c << 8) | d;
		return result;
	}

	public static int byte2int(byte b) {
		int l = b & 0x07f;
		if (b < 0) {
			l |= 0x80;
		}
		return l;
	}

	/**
	 * ip2long String类型IP转换为long类型(常用方法)
	 * 
	 * @param ip
	 * @return
	 * @throws UnknownHostException
	 */
	public static long ip2long(String ip) throws UnknownHostException {
		int ipNum = str2Ip(ip);
		return int2long(ipNum);
	}

	public static long int2long(int i) {
		long l = i & 0x7fffffffL;
		if (i < 0) {
			l |= 0x080000000L;
		}
		return l;
	}

	/**
	 * long2ip long类型转换为String类型(常用方法)
	 * 
	 * @param ip
	 * @return
	 */
	public static String long2ip(long ip) {
		int[] b = new int[4];
		b[0] = (int) ((ip >> 24) & 0xff);
		b[1] = (int) ((ip >> 16) & 0xff);
		b[2] = (int) ((ip >> 8) & 0xff);
		b[3] = (int) (ip & 0xff);
		String x;
		x = Integer.toString(b[0]) + "." + Integer.toString(b[1]) + "." + Integer.toString(b[2])
				+ "." + Integer.toString(b[3]);

		return x;

	}
}
