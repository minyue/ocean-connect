/**
 * 
 */
package org.ocean.connect.router.api.util;

import org.apache.commons.lang3.StringUtils;

/**
 * @author leiruiqi
 *
 */
public class TxCcSessionKeyUtil {
	
	public static String tx_session_separator = "|";

	public static String createTxSessionKey(String bizType,String userId,String deviceDomain,String deviceType,String deviceId){
		
		if(StringUtils.isBlank(bizType)||StringUtils.isBlank(userId)||StringUtils.isBlank(deviceDomain)){
			return null;
		}
		
		String websocket_username = bizType+tx_session_separator+userId+tx_session_separator+deviceDomain;
		
		websocket_username = websocket_username+tx_session_separator+deviceType+tx_session_separator+deviceId;
		
		return websocket_username;
	}
	
	public static String createTxSessionKeyUserIdPrefix(String bizType,String userIdPrefix){
		String sessionKey = bizType+tx_session_separator+userIdPrefix;
		return sessionKey;
	}
	public static String createTxSessionKeyUserId(String bizType,String userId){
		String sessionKey = bizType+tx_session_separator+userId+tx_session_separator;
		return sessionKey;
	}
	
	
}
