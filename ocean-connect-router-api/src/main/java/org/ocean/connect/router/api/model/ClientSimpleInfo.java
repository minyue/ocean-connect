/**
 * 
 */
package org.ocean.connect.router.api.model;

import java.io.Serializable;

/**
 * @author leiruiqi
 *
 */
public class ClientSimpleInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6029117260846479243L;

	private String crSessionKey;	
	
	private String deviceId;
	
	private String bizType;
	
	private String userId;


	public String getCrSessionKey() {
		return crSessionKey;
	}

	public void setCrSessionKey(String crSessionKey) {
		this.crSessionKey = crSessionKey;
	}

	public String getBizType() {
		return bizType;
	}

	public void setBizType(String bizType) {
		this.bizType = bizType;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	
	
}
