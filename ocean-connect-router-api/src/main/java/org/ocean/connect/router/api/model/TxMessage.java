/**
 * 
 */
package org.ocean.connect.router.api.model;

import java.io.Serializable;

/**
 * @author leiruiqi
 *
 */
public class TxMessage implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8672269938000219517L;

	private String type;
	
	private String action;	
	
	private String data;
	
	private String signId;
	
	
	
	/**
	 * 作为会话信息，cc默认加上的，返回给客户端的时候不需要
	 */
	private ClientSimpleInfo clientInfo;
	
	

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	/*private String bizDomain;
	
	private String bizName;
	
	public void action2BizSign(){
		if(StringUtils.isNotBlank(action)){
			String[] actionsplitArray = null;
			try {
				actionsplitArray = action.split("-");
			} catch (Exception e) {
				return;
			}
			if(actionsplitArray!=null&&actionsplitArray.length==2){
				bizDomain = actionsplitArray[0];
				bizName  =actionsplitArray[1];
			}
		}
	}
	public String getBizDomain() {
		return bizDomain;
	}

	public void setBizDomain(String bizDomain) {
		this.bizDomain = bizDomain;
	}

	public String getBizName() {
		return bizName;
	}

	public void setBizName(String bizName) {
		this.bizName = bizName;
	}*/

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSignId() {
		return signId;
	}

	public void setSignId(String signId) {
		this.signId = signId;
	}

	public ClientSimpleInfo getClientInfo() {
		return clientInfo;
	}

	public void setClientInfo(ClientSimpleInfo clientInfo) {
		this.clientInfo = clientInfo;
	}

	
	
}
