/**
 * 
 */
package org.ocean.connect.router.api;

import org.ocean.connect.router.api.model.TianxiaoConnectSession;
import org.ocean.connect.router.api.result.RouteBindResult;

/**
 * @author leiruiqi
 *
 */
public interface ConnectSessionBindApi {

	RouteBindResult bindSession(TianxiaoConnectSession session);
	
	RouteBindResult unbindSession(TianxiaoConnectSession session);
	
	RouteBindResult addBindAttribute(TianxiaoConnectSession session);
	
}
