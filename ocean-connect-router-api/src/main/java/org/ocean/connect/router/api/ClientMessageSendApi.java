/**
 * 
 */
package org.ocean.connect.router.api;

import org.ocean.connect.router.api.model.TianxiaoConnectSession;
import org.ocean.connect.router.api.model.TxMessage;
import org.ocean.connect.router.api.result.ClientMessageSendResult;

/**
 * @author leiruiqi
 *
 */
public interface ClientMessageSendApi {

	public ClientMessageSendResult sendMessage(TianxiaoConnectSession session,TxMessage txMessage);

}
