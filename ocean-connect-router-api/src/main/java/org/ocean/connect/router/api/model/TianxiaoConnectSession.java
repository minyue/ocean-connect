/**
 * 
 */
package org.ocean.connect.router.api.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author leiruiqi
 *
 */
public class TianxiaoConnectSession implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6384362355214560845L;

	/**
	 * 额外扩展属性放这里
	 */
	private Map<String,Object> extensions = new HashMap<String,Object>();
	
	/**
	 * 会话创建时间
	 */
	private Long createTime;
	
	/**
	 * 会话所在连接服务器的ip
	 */
	private String sessionIp;
	
	/**
	 * 根据规则建立的会话的Key
	 * 前缀-用户id-设备类型域id-设备类型id-设备编号
	 */
	private String sessionKey;
	
	/**
	 * cc 自动生成的websocket 的 id，用于校验会话的唯一性
	 * 
	 */
	private String sessionId;
	
	/**
	 * 用户id
	 */
	private String userId;
	
	/**
	 * 业务类型，不同业务使用不同的业务标志以区分
	 */
	private String bizType;
	
	/**
	 * 设备类型
	 */
	private String deviceType;
	
	/**
	 * 设备id，标识设备的唯一编号，用于区分多设备的关键属性
	 */
	private String deviceId;
	


	public Map<String, Object> getExtensions() {
		return extensions;
	}

	public void setExtensions(Map<String, Object> extensions) {
		this.extensions = extensions;
	}

	public String getSessionIp() {
		return sessionIp;
	}

	public void setSessionIp(String sessionIp) {
		this.sessionIp = sessionIp;
	}

	public String getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	public Long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getBizType() {
		return bizType;
	}

	public void setBizType(String bizType) {
		this.bizType = bizType;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	
}
