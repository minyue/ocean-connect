/**
 * 
 */
package org.ocean.connect.router.api.enums;


/**
 * @author leiruiqi
 * 
 * 设备领域类型
 */
public enum DeviceDomainEnum {

	//UNKNOW("0"),//考虑有可能是无设备域的通用性，不排除以后开放这个特性，没有传作用域的设置为通用域
	
	WEB_PC("1"),
	
	WEB_PAD("2"),
	
	MOBILE("3"),
	
	;
	
	private String code;
	
	
	public String getCode() {
		return code;
	}

	private DeviceDomainEnum(String code) {
		this.code = code;
	}

	public static DeviceDomainEnum getByName(String name){
		if(name == null||name.equals("")){
			return null;
		}
		for(DeviceDomainEnum domain:DeviceDomainEnum.values()){
			if(domain.name().equals(name)){
				return domain;
			}
		}
		return null;
	}
	public static DeviceDomainEnum getByCode(String code){
		if(code == null||code.equals("")){
			return null;
		}
		for(DeviceDomainEnum domain:DeviceDomainEnum.values()){
			if(domain.getCode().equals(code)){
				return domain;
			}
		}
		return null;
	}
	
}
