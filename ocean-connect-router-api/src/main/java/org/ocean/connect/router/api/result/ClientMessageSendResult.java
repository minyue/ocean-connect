/**
 * 
 */
package org.ocean.connect.router.api.result;

import org.ocean.connect.router.api.model.TxMessage;

/**
 * @author leiruiqi
 *
 */
public class ClientMessageSendResult extends BaseResult{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4296957296970741045L;
	
	private TxMessage txMessageArk;

	public TxMessage getTxMessageArk() {
		return txMessageArk;
	}

	public void setTxMessageArk(TxMessage txMessageArk) {
		this.txMessageArk = txMessageArk;
	}
	
	

}
