/**
 * 
 */
package org.ocean.connect.router.api.result;

import java.io.Serializable;

/**
 * @author leiruiqi
 *
 */
public class BaseResult implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3378209539312063022L;

	private boolean succese = false;
	
	private String code;
	
	private String msg;

	public boolean isSuccese() {
		return succese;
	}

	public void setSuccese(boolean succese) {
		this.succese = succese;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	
}
