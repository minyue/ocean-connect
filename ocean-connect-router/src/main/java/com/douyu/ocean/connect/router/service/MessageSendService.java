/**
 * 
 */
package com.douyu.ocean.connect.router.service;

import java.util.List;

import org.ocean.connect.router.api.model.TianxiaoConnectSession;

import com.douyu.ocean.connect.router.result.BaseResult;

/**
 * @author leiruiqi
 *
 */
public interface MessageSendService {

	public BaseResult messageSend(String bizType,String uid,String equipmentDomain,String deviceId,String messageJsonStr);
	
	public BaseResult messageSendByUidPrefix(String bizType,String uid,String messageJsonStr);
	
	public BaseResult messageSend(TianxiaoConnectSession session,String messageJsonStr);
	
	public BaseResult sendMessageFromDubble(TianxiaoConnectSession session, String messageJsonStr);
	
	public BaseResult sendMessageFromDubble(String bizType, String uid, String deviceDomain, String messageJsonStr) ;
	
	public BaseResult messageSendByWSChannel(String bizType, String uid, String deviceDomain, String messageJsonStr);
	
	public BaseResult messageSendByWSChannel(TianxiaoConnectSession session, String messageJsonStr);
	
	public BaseResult asyncMessageSend(String bizType,String uid,String equipmentDomain,String deviceId,String messageJsonStr);

	public BaseResult asyncMessageSend(TianxiaoConnectSession session, String messageJsonStr);
	
	public BaseResult asyncMessageSendBatch(String bizType,String uid,String deviceType, String equipmentDomain,List<String> deviceIds,String messageJsonStr);
	
	public BaseResult asyncMessageSendBatch(List<String> sessionKeys, String messageJsonStr);
}
