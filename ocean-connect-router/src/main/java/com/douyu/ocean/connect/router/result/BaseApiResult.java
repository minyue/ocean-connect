/**
 * 
 */
package com.douyu.ocean.connect.router.result;

/**
 * @author leiruiqi
 *
 */
public class BaseApiResult {

	private String code = "1";
	
	private String msg;
	
	private Object data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Object getData() {
		return data;
	}

	public void setDate(Object date) {
		this.data = date;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	
	
}
