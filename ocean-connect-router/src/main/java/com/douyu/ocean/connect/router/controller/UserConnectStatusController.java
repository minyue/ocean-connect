/**
 * 
 */
package com.douyu.ocean.connect.router.controller;

import java.util.List;

import javax.annotation.Resource;

import org.ocean.connect.router.api.model.TianxiaoConnectSession;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.douyu.ocean.connect.router.result.BaseApiResult;
import com.douyu.ocean.connect.router.service.ConnectBindService;

/**
 * @author leiruiqi
 *
 */

@RequestMapping("/connectStatus")
@RestController
public class UserConnectStatusController {

	@Resource
	private ConnectBindService connectBindService;
	
	
	@RequestMapping("/findUsersKeyByPrefix")
	public BaseApiResult findUsersKeyByPrefix(String bizType,String uidPrefix){
		BaseApiResult result = new BaseApiResult();
		
		List<String> sessionKeyList = connectBindService.findAllUserBindSessionKeyByPrefix(bizType, uidPrefix);
		result.setCode("1");
		result.setDate(sessionKeyList);
		
		return result;
	}
	
	@RequestMapping("/findUsersSessionByPrefix")
	public BaseApiResult findUserSessionByPrefix(String bizType,String uidPrefix){
		BaseApiResult result = new BaseApiResult();
		
		List<TianxiaoConnectSession> sessionList = connectBindService.findAllUserBindSessionByPrefix(bizType, uidPrefix);
		result.setCode("1");
		result.setDate(sessionList);
		
		return result;
	}
	
	@RequestMapping("/findUserSession")
	public BaseApiResult findUser(String bizType,String uid,String deviceDomain){
		BaseApiResult result = new BaseApiResult();
		
		List<TianxiaoConnectSession> sessionList = connectBindService.findAllUserBindSession(bizType, uid, deviceDomain);
		
		result.setCode("1");
		result.setDate(sessionList);
		
		return result;
	}
}
