/**
 * 
 */
package com.douyu.ocean.connect.router.service.impl;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.ocean.connect.router.api.enums.MessageBizDomain;
import org.ocean.connect.router.api.model.ClientSimpleInfo;
import org.ocean.connect.router.api.model.TianxiaoConnectSession;
import org.ocean.connect.router.api.model.TxMessage;
import org.ocean.connect.router.api.result.ClientMessageSendResult;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.alibaba.fastjson.JSON;
import com.douyu.ocean.connect.router.model.TxCrMessage;
import com.douyu.ocean.connect.router.service.ClientMessageSendService;
import com.douyu.ocean.connect.router.service.MessageSendService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author leiruiqi
 *
 */

@Service("clientMessageSendService")
@Slf4j
public class ClientMessageSendServiceImpl implements ClientMessageSendService {

	/*@Resource
	private ExternalInterfaceService externalInterfaceService;*/
	
	//private ConcurrentHashMap<String,String> messageDomainHostMap;

	@Value("${route.domain.txpush}")
	private String pushDomain;
	
	@Resource
	private MessageSendService messageSendService;
	@PostConstruct
	public void init(){
		
	}
	private void buildMessageRouteUrlMap(){
		
	}
	
	@Async
	@Override
	public ClientMessageSendResult messageSendSyn(TianxiaoConnectSession session, TxMessage txMessage) {
		
		TxCrMessage txCrMessage = new TxCrMessage(txMessage);
		txCrMessage.action2BizSign();
		String bizDomain = txCrMessage.getBizDomain();
		
		if(StringUtils.isBlank(bizDomain)){
			ClientMessageSendResult sendResult = new ClientMessageSendResult();
			sendResult.setSuccese(false);
			sendResult.setMsg("biz type not support");
		}
		
		if(StringUtils.equals(bizDomain, MessageBizDomain.cr.name())){
			ClientMessageSendResult sendResult = new ClientMessageSendResult();
			sendResult.setSuccese(false);
			sendResult.setMsg("biz type not support");
		}else if(StringUtils.equals(bizDomain, MessageBizDomain.txpush.name())){
			//ClientMessageSendResult sendResult = new ClientMessageSendResult();
			txCrMessage.setBizDomain(null);
			txCrMessage.setBizName(null);
			TxMessage txMessageArk = doTxPushBizRouteSyn(session,txCrMessage);
			String str = JSON.toJSONString(txMessageArk);
			//logger.info("txMessageArk str ="+str);
			messageSendService.messageSend(session, str);
		}else{
			ClientMessageSendResult sendResult = new ClientMessageSendResult();
			sendResult.setSuccese(false);
			sendResult.setMsg("biz type not support");
		}
		return null;
	}

	private TxMessage doTxPushBizRouteSyn(TianxiaoConnectSession session, TxMessage txMessage){
		ClientSimpleInfo clientInfo = new ClientSimpleInfo();
		clientInfo.setBizType(session.getBizType());
		clientInfo.setUserId(session.getUserId());
		clientInfo.setDeviceId(session.getDeviceId());
		clientInfo.setCrSessionKey(session.getSessionKey());
		txMessage.setClientInfo(clientInfo);
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    MediaType type = MediaType.parseMediaType("application/json; charset=UTF-8");
	    headers.setContentType(type);
	    headers.add("Accept", MediaType.APPLICATION_JSON.toString());
	    
		String uri = "http://"+pushDomain+"/txCr/bizMessageSyn";
		String jsonStr = JSON.toJSONString(txMessage);
		log.info("uri="+uri);
		log.info("jsonStr="+jsonStr);
		HttpEntity<String> formEntity = new HttpEntity<String>(jsonStr, headers);
	    TxMessage txMessageArk = restTemplate.postForObject(uri, formEntity,TxMessage.class);
		log.info("get txMessageArk from txpush result="+JSON.toJSONString(txMessageArk));
	    if(txMessageArk!=null){
	    	txMessageArk.setClientInfo(null);
	    }
		return txMessageArk;
	}
}
