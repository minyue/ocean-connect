 /**
 * Baijiahulian.com Inc.
 * Copyright (c) 2014-2016 All Rights Reserved.
 */

package com.douyu.ocean.connect.router.cacheClient.impl;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import com.douyu.ocean.connect.router.cacheClient.CacheClient;


/**
 * @title RedisClientImpl
 * @desc TODO 
 * @author leiruiqi
 * @date 2016年1月12日
 * @version 1.0
 */
    
public class RedisClientImpl implements CacheClient {

	@Resource
	private RedisTemplate<String, Object> businessRedisTemplate;

	public RedisTemplate<String, Object> getBusinessRedisTemplate() {
		return businessRedisTemplate;
	}

	public void setBusinessRedisTemplate(
			RedisTemplate<String, Object> businessRedisTemplate) {
		this.businessRedisTemplate = businessRedisTemplate;
	}

	public void set(String key, Object value, long timeout, TimeUnit unit) {
		if(StringUtils.isBlank(key)){
			return;
		}
		ValueOperations<String, Object> valueops = businessRedisTemplate
				.opsForValue();
		valueops.set(key, value, timeout, unit);
	}

	public Object getValue(String key) {
		if(StringUtils.isBlank(key)){
			return null;
		}
		ValueOperations<String, Object> valueops = businessRedisTemplate
				.opsForValue();
		Object value = valueops.get(key);
		return value;
	}

	public void delete(String key) {
		if(StringUtils.isBlank(key)){
			return;
		}
		businessRedisTemplate.delete(key);
	}

	/**
	 * 批量删除
	 */
	public void delete(Collection<String> keys) {
		if(keys == null || keys.size()<0){
			return;
		}
		businessRedisTemplate.delete(keys);
	}

	public Boolean exists(String key) {
		if(StringUtils.isBlank(key)){
			return false;
		}
		return businessRedisTemplate.hasKey(key);
	}

	// 如果这个key不在redis中，就设置，并缓存一段时间。
	// TODO
	// 问题，如果setIfAbsent执行之后，没有设置过期时间，且此时，服务器挂了，那么后面的设置缓存时间就失败了，然后这个key一直缓存在redis中。
	public boolean setIfAbsent(String key, Object value, long timeout) {
		ValueOperations<String, Object> valueops = businessRedisTemplate
				.opsForValue();
		boolean result = valueops.setIfAbsent(key, value);
		if (result) {
			this.set(key, value, timeout, TimeUnit.SECONDS);
		}
		return result;
	}

	//@Override
	public List<Object> getMulti(List<String> keys) {
		// TODO Auto-generated method stub
		return businessRedisTemplate.opsForValue().multiGet(keys);
	}

	public Object[] getMulti(String[] keys) {
		ValueOperations<String, Object> valueops = businessRedisTemplate
				.opsForValue();
		List<String> keys_list = Arrays.asList(keys);
		List<Object> value = valueops.multiGet(keys_list);
		return value.toArray();
	}

	@Override
	public void set(String key, Object value, long timeout) {
		ValueOperations<String, Object> valueops = businessRedisTemplate
				.opsForValue();
		valueops.set(key, value, timeout, TimeUnit.SECONDS);
	}
	
	@Override
	public void set(String key, Object value) {
		ValueOperations<String, Object> valueops = businessRedisTemplate
				.opsForValue();
		valueops.set(key, value);
	}

	@Override
	public Set<String> getkeys(String pattern) {
		Set<String> keys = businessRedisTemplate.keys(pattern);
		return keys;
	}
	

	@Override
	public void addOneForSet(String key, Object value) {
		businessRedisTemplate.opsForSet().add(key, value);
	}

	@Override
	public void delOneForSet(String key, Object value) {
		businessRedisTemplate.opsForSet().remove(key, value);
	}

	@Override
	public void addForSet(String key, Object... values) {
		businessRedisTemplate.opsForSet().add(key, values);
	}

	@Override
	public void delForSet(String key, Object... values) {
		businessRedisTemplate.opsForSet().remove(key, values);
	}

	@Override
	public Set<Object> getSet(String key) {
		return businessRedisTemplate.opsForSet().members(key);
	}

	
}
