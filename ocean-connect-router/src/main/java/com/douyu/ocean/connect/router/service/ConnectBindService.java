/**
 * 
 */
package com.douyu.ocean.connect.router.service;

import java.util.List;

import org.ocean.connect.router.api.model.TianxiaoConnectSession;
/**
 * @author leiruiqi
 *
 */
public interface ConnectBindService {

	void bindSession(TianxiaoConnectSession session);
	
	boolean addBindAttribute(TianxiaoConnectSession session);
	
	void unbindSession(TianxiaoConnectSession session);
	
	TianxiaoConnectSession findBySessionKey(String key);
	
	List<TianxiaoConnectSession> findAllUserBindSession(String bizType,String userId,String DeviceDomain);
	
	//TianxiaoConnectSession findUserBindSession(String bizType,String userId,String DeviceDomain);
	
	List<TianxiaoConnectSession> findAllUserBindSessionByPrefix(String bizType,String userIdPrefix);
	
	List<String> findAllUserBindSessionKeyByPrefix(String bizType,String userIdPrefix);
	
}
