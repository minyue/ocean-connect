/**
 * 
 */
package com.douyu.ocean.connect.router.api.impl;

import javax.annotation.Resource;

import org.ocean.connect.router.api.ConnectSessionBindApi;
import org.ocean.connect.router.api.model.TianxiaoConnectSession;
import org.ocean.connect.router.api.result.RouteBindResult;
import org.springframework.stereotype.Service;

import com.douyu.ocean.connect.router.service.ConnectBindService;
import com.douyu.ocean.connect.router.service.ConnectBindTokenService;

/**
 * @author leiruiqi
 *
 */
@Service("connectSessionBindApi")
public class ConnectSessionBindApiImpl implements ConnectSessionBindApi{

	@Resource
	private ConnectBindService connectBindService;
	
	@Resource
	private ConnectBindTokenService connectBindTokenService;
	
	@Override
	public RouteBindResult bindSession(TianxiaoConnectSession session) {
		RouteBindResult result = new RouteBindResult();
		connectBindService.bindSession(session);
		result.setSuccese(true);
		return result;
	}

	@Override
	public RouteBindResult unbindSession(TianxiaoConnectSession session) {
		RouteBindResult result = new RouteBindResult();
		connectBindService.unbindSession(session);
		result.setSuccese(true);
		return result;	
	}

	@Override
	public RouteBindResult addBindAttribute(TianxiaoConnectSession session) {
		RouteBindResult result = new RouteBindResult();
		boolean bool = connectBindService.addBindAttribute(session);
		result.setSuccese(bool);
		return result;	
		
	}

	
	

}
