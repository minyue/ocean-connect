package com.douyu.ocean.connect.router.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class TianxiaoRouteNormalInterceptor extends HandlerInterceptorAdapter{

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		//方便提前上线，注释内网拦截器处理，直接返回true
		//return InnerTokenUtil.checkInnerIpOrToken(request);
		return true;
	}
}
