package com.douyu.ocean.connect.router.utils;

import com.douyu.ocean.config.client.PropertiesConfigClientFactoryBean;

import java.util.Properties;

import javax.annotation.PostConstruct;


public final class PropertiesUtils {


    public static String PROJECT_GROUP_CODE = "ocean";
    public static String PROJECT_NAME = "ocean-config-cache";
    public static String PROJECT_MODULES = "jdbc-mysql,config"; // 多个用,分隔
    public static String JDBC_MYSQL_DRIVER_KEY = "jdbc.driverClassName";
    public static String JDBC_STT_BIGDATA_URL_KEY = "jdbc_stt_bigdata.url";
    public static String JDBC_PORTRAIT_URL_KEY = "jdbc_portrait.url";

    private static Properties props;

    public synchronized final static String get(String key) throws Exception {
        if (null == props) {
            props = new PropertiesConfigClientFactoryBean(PROJECT_GROUP_CODE, PROJECT_NAME, PROJECT_MODULES).getObject();
        }
        return props.getProperty(key);
    }
    public synchronized final static int getInt(String key) throws Exception {
    	String value = get(key);
        return Integer.parseInt(value);
    }
    
    @PostConstruct
    public static void init()throws Exception {
    	props = new PropertiesConfigClientFactoryBean(PROJECT_GROUP_CODE, PROJECT_NAME, PROJECT_MODULES).getObject();
    }

}
