/**
 * 
 */
package com.douyu.ocean.connect.router.service;

import java.util.Map;

/**
 * @author leiruiqi
 *
 */
public interface ConnectBindTokenService {

	public boolean bindTokenCheck(String tokenType,Map<String,String> Params);
	
}
