 /**
 * Copyright (c) 2014-2016 All Rights Reserved.
 */

package com.douyu.ocean.connect.router.cacheClient;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface CacheClient {

	public List<Object> getMulti(List<String> keys);

	public Object[] getMulti(String[] keys);

	public void set(String key, Object value, long timeout);

	public Object getValue(String key);

	public void delete(String key);

	public void delete(Collection<String> keys);

	public Boolean exists(String key);

	//如果不存在该key ，则正常写入缓存并返回true，否则返回 false
	public boolean setIfAbsent(String key, Object value, long timeout);
	
	public void set(String key, Object value);
	
	public Set<String>  getkeys(String pattern);
	
	public void addOneForSet(String key,Object value);
	
	public void delOneForSet(String key,Object value);
	
	public void addForSet(String key ,Object ...values);
	
	public void delForSet(String key ,Object ...values);
	
	public Set<Object> getSet(String key);

}
