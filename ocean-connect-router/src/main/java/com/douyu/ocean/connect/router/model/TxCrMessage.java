/**
 * 
 */
package com.douyu.ocean.connect.router.model;

import org.apache.commons.lang3.StringUtils;
import org.ocean.connect.router.api.model.TxMessage;

/**
 * @author leiruiqi
 *
 */
public class TxCrMessage extends TxMessage{

	private String bizDomain;
	
	private String bizName;
	
	public TxCrMessage() {
		
	}
	
	public TxCrMessage(TxMessage txMessage) {
		this.setType(txMessage.getType());
		this.setAction(txMessage.getAction());
		this.setClientInfo(txMessage.getClientInfo());
		this.setData(txMessage.getData());
		this.setSignId(txMessage.getSignId());
	}
	
	public void action2BizSign(){
		if(StringUtils.isNotBlank(getAction())){
			String[] actionsplitArray = null;
			try {
				actionsplitArray = getAction().split("-");
			} catch (Exception e) {
				return;
			}
			if(actionsplitArray!=null&&actionsplitArray.length==2){
				bizDomain = actionsplitArray[0];
				bizName  =actionsplitArray[1];
			}
		}
	}

	public String getBizDomain() {
		return bizDomain;
	}

	public void setBizDomain(String bizDomain) {
		this.bizDomain = bizDomain;
	}

	public String getBizName() {
		return bizName;
	}

	public void setBizName(String bizName) {
		this.bizName = bizName;
	}
	
	
}
