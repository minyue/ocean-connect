/**
 * 
 */
package com.douyu.ocean.connect.router.api.impl;

import javax.annotation.Resource;

import org.ocean.connect.router.api.ClientMessageSendApi;
import org.ocean.connect.router.api.model.TianxiaoConnectSession;
import org.ocean.connect.router.api.model.TxMessage;
import org.ocean.connect.router.api.result.ClientMessageSendResult;
import org.springframework.stereotype.Service;

import com.douyu.ocean.connect.router.service.ClientMessageSendService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author leiruiqi
 *
 */

@Service("clientMessageSendApi")
@Slf4j
public class ClientMessageSendApiImpl implements ClientMessageSendApi {

	@Resource
	private ClientMessageSendService clientMessageSendService;
	
	@Override
	public ClientMessageSendResult sendMessage(TianxiaoConnectSession session, TxMessage txMessage) {
		log.info("ClientMessageSendApi.sendMessage excute");
		ClientMessageSendResult result = new ClientMessageSendResult();
		clientMessageSendService.messageSendSyn(session, txMessage);
		result.setSuccese(true);
		return result;
	}

}
