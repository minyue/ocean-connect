/**
 * 
 */
package com.douyu.ocean.connect.router.service;

import org.ocean.connect.router.api.model.TianxiaoConnectSession;
import org.ocean.connect.router.api.model.TxMessage;
import org.ocean.connect.router.api.result.ClientMessageSendResult;

/**
 * @author leiruiqi
 *
 */
public interface ClientMessageSendService {

	
	ClientMessageSendResult messageSendSyn(TianxiaoConnectSession session,TxMessage txMessage);
}
