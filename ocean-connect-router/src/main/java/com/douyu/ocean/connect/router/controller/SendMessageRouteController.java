/**
 * 
 */
package com.douyu.ocean.connect.router.controller;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.alibaba.dubbo.common.utils.StringUtils;
import com.douyu.ocean.connect.router.result.BaseApiResult;
import com.douyu.ocean.connect.router.result.BaseResult;
import com.douyu.ocean.connect.router.service.MessageSendService;

/**
 * @author leiruiqi
 *
 * 发送消息
 * 
 */
@RequestMapping("/messageRoute")
@RestController
@Slf4j
public class SendMessageRouteController {
	
	@Resource
	MessageSendService messageSendService;
	

	@RequestMapping("/txSendSingle")
	public BaseApiResult txSendSingle(String bizType,String uid,String deviceDomain,String deviceId,String message) {
		BaseApiResult result = new BaseApiResult();
		
		BaseResult baseResult = messageSendService.messageSend(bizType, uid, deviceDomain,deviceId, message);
		
		//BaseResult baseResult = messageSendService.sendMessageFromDubble(bizType, uid, deviceDomain, message);
		if(!baseResult.isSuccese()){			
			result.setCode("0");
			result.setMsg(baseResult.getMsg());			
		}
		log.info("txSendSingle bizType="+bizType+",uid="+uid+",deviceDoamin="+deviceDomain+",deviceId="+deviceId+", resultCode="+result.getCode()+",msg="+result.getMsg());
		return result;
	}
	
	@RequestMapping("/txSendSingleByUidprefix")
	public BaseApiResult txSendSingleByUidprefix(String bizType,String uid,String message) {
		BaseApiResult result = new BaseApiResult();
		
		
		BaseResult baseResult = messageSendService.messageSendByUidPrefix(bizType, uid, message);
		
		if(!baseResult.isSuccese()){			
			result.setCode("0");
			result.setMsg(baseResult.getMsg());			
		}
		log.info("txSendSingle bizType="+bizType+",uid="+uid+", resultCode="+result.getCode()+",msg="+result.getMsg());
		return result;
	}
	
	@RequestMapping("/txAsyncSendSingle")
	public BaseApiResult txAsyncSendSingle(String bizType,String uid,String deviceDomain,String deviceId,String message) {
		BaseApiResult result = new BaseApiResult();
		
		BaseResult baseResult = messageSendService.messageSend(bizType, uid, deviceDomain,deviceId, message);
		
		//BaseResult baseResult = messageSendService.sendMessageFromDubble(bizType, uid, deviceDomain, message);
		if(!baseResult.isSuccese()){			
			result.setCode("0");
			result.setMsg(baseResult.getMsg());			
		}
		log.info("txAsyncSendSingle bizType="+bizType+",uid="+uid+",deviceDoamin="+deviceDomain+",deviceId="+deviceId+", resultCode="+result.getCode()+",msg="+result.getMsg());
		return result;
	}
	
	@RequestMapping("/txAsyncSendBatch")
	public BaseApiResult txAsyncSendBatch(String bizType, String uid, String deviceType, String deviceDomain,
			String deviceIds, String message) {
		log.debug("txAsyncSendBatch request bizType=" + bizType + ",uid=" + uid + ",deviceDoamin=" + deviceDomain + ",deviceIds="
				+ deviceIds+",deviceType="+deviceType+", message="+message);
		BaseApiResult result = new BaseApiResult();
		if (StringUtils.isBlank(deviceIds)) {
			result.setCode("1");
			result.setMsg("deviceIds must not be empty!");
			return result;
		}
		BaseResult baseResult = null;
		try {
			baseResult = messageSendService.asyncMessageSendBatch(bizType, uid, deviceType, deviceDomain, Arrays.asList(deviceIds.split(",")),
					message);
		} catch (Exception e) {
			log.error("txAsyncSendBatch error!", e);
		}

		if (!baseResult.isSuccese()) {
			result.setCode("0");
			result.setMsg(baseResult.getMsg());
		}
		log.info("txAsyncSendBatch bizType=" + bizType + ",uid=" + uid + ",deviceDoamin=" + deviceDomain + ",deviceIds="
				+ deviceIds + ", resultCode=" + result.getCode() + ",msg=" + result.getMsg());
		return result;
	}
	
	public static void main(String[] args) {
		RestTemplate restTemplate = new RestTemplate();
		MultiValueMap<String, Object> params = new LinkedMultiValueMap<String, Object>();

		
	    params.add("uid", "337f4e84-fa51-453c-a872-d2ef92d6f456");
	    params.add("bizType", "QR_LOGIN");
	    params.add("deviceDomain", "1");
	    params.add("message", "1");
	    String uri = "http://tx-cc:16088/messageRoute/txSendSingle/";
	   // String uri = "http://test-txcr.ctest.baijiahulian.com/messageRoute/txSendSingle/";
	   // String uri = "http://cr.service.tianxiao100.com/messageRoute/txSendSingle";
		
	    BaseApiResult sendresult = restTemplate.postForObject(uri, params, BaseApiResult.class);
		System.out.println(sendresult.getCode());
		System.out.println(sendresult.getMsg());
	}
}
