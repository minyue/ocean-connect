/**
 * 
 */
package com.douyu.ocean.connect.router.result;

/**
 * @author leiruiqi
 *
 */
public class BaseResult {

	private boolean succese;
	
	private String code;
	
	private String msg;
	
	private Object data;

	public boolean isSuccese() {
		return succese;
	}

	public void setSuccese(boolean succese) {
		this.succese = succese;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
	
}
