/**
 * 
 */
package com.douyu.ocean.connect.router.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author leiruiqi
 *
 */
public class TianxiaoConnectionSession1 implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6384362355214560845L;

	/**
	 * 额外扩展属性放这里
	 */
	private Map<String,Object> extensions = new HashMap<String,Object>();
	
	/**
	 * 会话创建时间
	 */
	private Long createTime;
	
	/**
	 * 会话所在连接服务器的ip
	 */
	private String sessionIp;
	
	/**
	 * 根据规则建立的会话的Key
	 * 前缀-机构id-用户id-设备类型id
	 */
	private String sessionKey;
	


	public Map<String, Object> getExtensions() {
		return extensions;
	}

	public void setExtensions(Map<String, Object> extensions) {
		this.extensions = extensions;
	}

	
	
	public String getSessionIp() {
		return sessionIp;
	}

	public void setSessionIp(String sessionIp) {
		this.sessionIp = sessionIp;
	}

	public String getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	public Long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}
	
	
	
}
