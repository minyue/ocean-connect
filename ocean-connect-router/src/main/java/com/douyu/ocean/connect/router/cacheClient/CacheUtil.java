
 /**
 * Copyright (c) 2014-2016 All Rights Reserved.
 */
    
package com.douyu.ocean.connect.router.cacheClient;

import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * @title CacheUtil
 * @desc TODO 
 * @author leiruiqi
 * @date 2016年1月15日
 * @version 1.0
 */

@Component
public class CacheUtil {

    @Resource
    private CacheClient defaultCache;
    
    private static CacheUtil cacheUtil;
    
    @PostConstruct
    public void init(){
        cacheUtil = this;
        cacheUtil.defaultCache = this.defaultCache;
    }
    
    public static List<Object> getMulti(List<String> keys){
        return cacheUtil.defaultCache.getMulti(keys);
    }

    public static Object[] getMulti(String[] keys){
        return cacheUtil.defaultCache.getMulti(keys);
    }

    public static void set(String key, Object value, long timeout){
        cacheUtil.defaultCache.set(key, value, timeout);
    }

    public static Object getValue(String key){
        return cacheUtil.defaultCache.getValue(key);
    }

    public static void delete(String key){
        cacheUtil.defaultCache.delete(key);
    }

    public static void delete(Collection<String> keys){
        cacheUtil.defaultCache.delete(keys);
    }

    public static Boolean exists(String key){
        return cacheUtil.defaultCache.exists(key);
    }

    //如果不存在该key ，则正常写入缓存并返回true，否则返回 false
    public static boolean setIfAbsent(String key, Object value, long timeout){
        return cacheUtil.defaultCache.setIfAbsent(key, value, timeout);
    }
    
    public static void set(String key, Object value){
        cacheUtil.defaultCache.set(key, value);
    }
    
    public static Set<String>  getkeys(String pattern){
        return cacheUtil.defaultCache.getkeys(pattern);
    }
}

    